import 'package:bmi_calculator/app_configs/state_notifier/app_state_notifier.dart';
import 'package:bmi_calculator/app_configs/themes/app_theme.dart';
import 'package:bmi_calculator/routes/routes.dart' as route;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'routes/router_names.dart';

void main() => runApp(
      ChangeNotifierProvider<AppStateNotifier>(
        create: (BuildContext context) => AppStateNotifier(),
        //  OR
        /*create: (BuildContext context) {
        return AppStateNotifier();
      },*/
        child: MyApp(),
      ),
    );

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateNotifier>(
      builder: (context, appState, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: "BMI Calculator",

          //  App Theme.
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          themeMode: (appState.isDarkModeOn != null && appState.isDarkModeOn)
              ? ThemeMode.dark
              : ThemeMode.light,

          //  Page Routing.
          initialRoute: appHomePage,
          onGenerateRoute: route.generateRoute,
        );
      },
    );
  }
}
