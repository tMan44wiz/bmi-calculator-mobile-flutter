import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Create a class and call it anything, then extend it to "ChangeNotifier".
class AppStateNotifier extends ChangeNotifier {
  // Initialize your states variable here.
  bool _isDarkModeOn;
  String _selectedUnit;
  String _notification;

  int _age;
  double _height;
  double _weight;

  //  Constructor
  AppStateNotifier() {
    _isDarkModeOn = false;
    _selectedUnit = "ImperialUnit";
    _notification = "Yes";

    _age = 0;
    _height = 0.0;
    _weight = 0.0;

    getThemeMode();
    getSelectedUnit();
    getNotificationStatus();
  }

  //  GETTERS
  int get age => _age;
  double get height => _height;
  double get weight => _weight;

  bool get isDarkModeOn => _isDarkModeOn;
  String get selectedUnit => _selectedUnit;
  String get notification => _notification;

  // SETTERS
  //  Height Value Setter
  void updateHeightValue(double newHeightValue) {
    _height = (newHeightValue * 10).roundToDouble() / 10;
    notifyListeners();
  }

  //  Theme Setter
  void updateTheme(bool isDarkModeTrueOrFalse) {
    _isDarkModeOn = isDarkModeTrueOrFalse; // update state variable
    notifyListeners(); // Call the "notifyListeners()" Function to trigger a notification to the App.
    saveThemeMode(); //  Save the Theme Mode to SharedPreferences
  }

  //  Unit Setter
  void updateUnit(String unit) {
    _selectedUnit = unit;
    notifyListeners();
    saveSelectedUnit();
  }

  //  Notification Setter
  void updateNotification(String notification) {
    _notification = notification;
    notifyListeners();
    saveNotificationStatus();
  }

/*  ///////////////////////////////////////////////////////////////////
*   /////////////////// THE SHARED PREFERENCES  ///////////////////////
*   ///////////////////////////////////////////////////////////////////
*/
  saveThemeMode() async {
    SharedPreferences saveThemeModePreference =
        await SharedPreferences.getInstance();
    await saveThemeModePreference.setBool("themeKey", _isDarkModeOn);
  }

  getThemeMode() async {
    SharedPreferences saveThemeModePreference =
        await SharedPreferences.getInstance();
    bool prefsIsDarkModeOn = saveThemeModePreference.getBool("themeKey");

    if (prefsIsDarkModeOn == null) {
      updateTheme(_isDarkModeOn);
    } else {
      updateTheme(prefsIsDarkModeOn);
    }
  }

  saveSelectedUnit() async {
    SharedPreferences savedUnitSharedPreference =
        await SharedPreferences.getInstance();
    savedUnitSharedPreference.setString("UnitKey", _selectedUnit);
  }

  getSelectedUnit() async {
    SharedPreferences savedUnitSharedPreference =
        await SharedPreferences.getInstance();
    String prefsSelectedUnit = savedUnitSharedPreference.getString("UnitKey");

    if (prefsSelectedUnit == null || prefsSelectedUnit.isEmpty) {
      updateUnit(_selectedUnit);
    } else {
      updateUnit(prefsSelectedUnit);
    }
  }

  saveNotificationStatus() async {
    SharedPreferences savedNotificationStatus =
        await SharedPreferences.getInstance();
    savedNotificationStatus.setString("notificationKey", _notification);
  }

  getNotificationStatus() async {
    SharedPreferences savedNotificationStatus =
        await SharedPreferences.getInstance();
    String prefsNotification =
        savedNotificationStatus.getString("notificationKey");

    if (prefsNotification == null || prefsNotification.isEmpty) {
      updateNotification(_notification);
    } else {
      updateNotification(prefsNotification);
    }
  }
}

/*
* Go to the "main.dart" file which happens to be the starting phase of the App
* and wrap "MyApp" with a "ChangeNotifierProvider<>()" Function.
* */
