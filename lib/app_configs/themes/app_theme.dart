import 'package:flutter/material.dart';


class AppTheme {
  AppTheme._();

  //  Initializing Light Color Theme.
  static const Color _lightPrimaryColor = Color(0xFFF5F5F5);
  static const Color _lightPrimaryVariantColor = Color(0xFFEEEEEE);
  static const Color _lightSecondaryColor = Color(0xFFFA0379);
  static const Color _lightSecondaryVariantColor = Color(0xFFFFDFEE);
  static const Color _lightTextColor = Color(0xFF10093D);
  static const Color _lightIconColor = Color(0xFF10093D);
  static const Color _lightGrayColor = Color(0x42000000);


  //  Initializing Dark Color Theme.
  static const Color _darkPrimaryColor = Color(0xFF10093D);
  //static const Color _darkPrimaryVariantColor = Color(0xFF1B1061);
  static const Color _darkSecondaryColor = Color(0xFFFA0379);
  static const Color _darkSecondaryVariantColor = Color(0x3DFFFFFF);
  static const Color _darkTextColor = Color(0xFFF5F5F5);


  //  Light Theme Configurations
  static final ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: _lightPrimaryColor,
    appBarTheme: AppBarTheme(
      elevation: 1.0,
      color: _lightPrimaryVariantColor,
      iconTheme: IconThemeData(color: _lightIconColor),
      textTheme: TextTheme(title: TextStyle(color: _lightTextColor, fontSize: 20.0, fontWeight: FontWeight.w600))
    ),
    cardTheme: CardTheme(color: _lightSecondaryVariantColor, elevation: 6.0),
    iconTheme: IconThemeData(color: _lightIconColor),
    buttonTheme: ButtonThemeData(buttonColor: _lightSecondaryColor),
    colorScheme: ColorScheme.light(primaryVariant: _lightPrimaryVariantColor, background: _lightSecondaryVariantColor,),
    textTheme: _lightTextTheme,
  );
  static final TextTheme _lightTextTheme = TextTheme(
    caption: _lightCaptionTextStyle,
    subtitle: _lightSubtitleTextStyle,
    display1: _lightDisplay1TextStyle,
    display2: _lightDisplay2TextStyle,
    display3: _lightDisplay3TextStyle,
    display4: _lightDisplay4TextStyle,
    body1: _lightBody1TextStyle,
    body2: _lightBody2TextStyle,
    button: _lightButtonTextStyle,
  );
  static final TextStyle _lightCaptionTextStyle =  TextStyle(color: _lightTextColor, fontSize: 18.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway');
  static final TextStyle _lightSubtitleTextStyle =  TextStyle(color: _lightTextColor, fontSize: 14.0, fontWeight: FontWeight.w800, fontFamily: 'Raleway');
  static final TextStyle _lightDisplay1TextStyle =  TextStyle(color: _lightTextColor, fontSize: 64.0, fontWeight: FontWeight.w700, fontFamily: 'Raleway');
  static final TextStyle _lightDisplay2TextStyle =  TextStyle(color: _lightGrayColor, fontSize: 42.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway');
  static final TextStyle _lightDisplay3TextStyle =  TextStyle(color: _lightTextColor, fontSize: 42.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway');
  static final TextStyle _lightDisplay4TextStyle =  TextStyle(color: _lightTextColor, fontSize: 26.0, fontWeight: FontWeight.w800, fontFamily: 'Raleway');
  static final TextStyle _lightBody1TextStyle =  TextStyle(color: _lightTextColor, fontSize: 14.0, fontWeight: FontWeight.w800, fontFamily: 'Raleway', height: 1.5);
  static final TextStyle _lightBody2TextStyle =  TextStyle(color: _lightTextColor, fontSize: 13.0, fontFamily: 'Raleway', height: 1.5);
  static final TextStyle _lightButtonTextStyle =  TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway',);


  //  Dark Theme Configurations
  static final ThemeData darkTheme = ThemeData(
    scaffoldBackgroundColor: _darkPrimaryColor,
    appBarTheme: AppBarTheme(
      color: _darkPrimaryColor,
      iconTheme: IconThemeData(color: _darkPrimaryColor),
      textTheme: TextTheme(title: TextStyle(color: _darkTextColor, fontSize: 20.0, fontWeight: FontWeight.w600))
    ),
    cardTheme: CardTheme(color: _darkSecondaryVariantColor, elevation: 6.0),
    iconTheme: IconThemeData(color: _darkSecondaryVariantColor),
    buttonTheme: ButtonThemeData(buttonColor: _darkSecondaryColor),
    colorScheme: ColorScheme.light(primaryVariant: _darkPrimaryColor, background: _darkSecondaryVariantColor),
    textTheme: _darkTextTheme,
  );
  static final TextTheme _darkTextTheme = TextTheme(
    caption: _darkCaptionTextStyle,
    subtitle: _darkSubtitleTextStyle,
    display1: _darkDisplay1TextStyle,
    display2: _darkDisplay2TextStyle,
    display3: _darkDisplay3TextStyle,
    display4: _darkDisplay4TextStyle,
    body1: _darkBody1TextStyle,
    body2: _darkBody2TextStyle,
    button: _darkButtonTextStyle,
  );
  static final TextStyle _darkCaptionTextStyle =  TextStyle(color: _darkTextColor, fontSize: 18.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway');
  static final TextStyle _darkSubtitleTextStyle =  TextStyle(color: _darkTextColor, fontSize: 14.0, fontWeight: FontWeight.w800, fontFamily: 'Raleway');
  static final TextStyle _darkDisplay1TextStyle =  TextStyle(color: _darkTextColor, fontSize: 64.0, fontWeight: FontWeight.w700, fontFamily: 'Raleway');
  static final TextStyle _darkDisplay2TextStyle =  TextStyle(color: _darkSecondaryVariantColor, fontSize: 42.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway');
  static final TextStyle _darkDisplay3TextStyle =  TextStyle(color: _darkTextColor, fontSize: 42.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway');
  static final TextStyle _darkDisplay4TextStyle =  TextStyle(color: _darkTextColor, fontSize: 26.0, fontWeight: FontWeight.w800, fontFamily: 'Raleway');
  static final TextStyle _darkBody1TextStyle =  TextStyle(color: _darkTextColor, fontSize: 14.0, fontWeight: FontWeight.w800, fontFamily: 'Raleway', height: 1.5);
  static final TextStyle _darkBody2TextStyle =  TextStyle(color: _darkTextColor, fontSize: 13.0, fontFamily: 'Raleway', height: 1.5);
  static final TextStyle _darkButtonTextStyle =  TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.w600, fontFamily: 'Raleway',);
}