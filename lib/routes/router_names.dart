const String appHomePage = "/";
const String bmiResultPage = "/bmiResult";
const String bmiHintPage = "/bmiHint";
const String appSettings = "/appSetting";