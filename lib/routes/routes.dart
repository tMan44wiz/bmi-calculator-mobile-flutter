import 'package:flutter/material.dart';
import 'router_names.dart';

import 'package:bmi_calculator/pages/app_home_screen.dart';
import 'package:bmi_calculator/pages/app_settings.dart';
import 'package:bmi_calculator/pages/bmi_hint_screen.dart';
import 'package:bmi_calculator/pages/bmi_result_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {

  String routeName = settings.name;
  final routerArguments = settings.arguments;

  switch (routeName) {
    case appHomePage:
      return MaterialPageRoute(builder: (context) => AppHome());
      break;

    case bmiResultPage:
      return MaterialPageRoute(builder: (context) => BMIResult(routerArguments: routerArguments));
      break;

    case bmiHintPage:
      return MaterialPageRoute(builder: (context) => BMIHint(routerArgument: routerArguments,));
      break;

    default:
      return MaterialPageRoute(builder: (context) => AppSettings());
      break;
  }
}