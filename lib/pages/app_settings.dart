import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:bmi_calculator/app_configs/state_notifier/app_state_notifier.dart';


class AppSettings extends StatefulWidget {
  @override
  _AppSettingsState createState() => _AppSettingsState();
}

class _AppSettingsState extends State<AppSettings> {

  bool _isImperialUnit;
  bool _isMetricUnit;
  bool _isNotificationYes;
  bool _isNotificationNo;
  String _existingUnit;
  String _existingNotification;

  //  Change from one Unit to another.
  void _changeUnit(bool setUnitVisibility) {
    if(setUnitVisibility == _isImperialUnit) {
      setState(() {
        _isImperialUnit = true;
        _isMetricUnit = false;
      });
    }
    else {
      setState(() {
        _isImperialUnit = false;
        _isMetricUnit = true;
      });
    }
  }

  //  Change Notification state
  void _changeNotification(bool setNotificationVisibility) {
    if(setNotificationVisibility == _isNotificationYes) {
      setState(() {
        _isNotificationYes = true;
        _isNotificationNo = false;
      });
    }
    else {
      setState(() {
        _isNotificationYes = false;
        _isNotificationNo = true;
      });
    }
  }


  @override
  void didChangeDependencies() async{
    super.didChangeDependencies();
    AppStateNotifier appStateProvider = Provider.of<AppStateNotifier>(context);
    _existingUnit = appStateProvider.selectedUnit;
    _existingNotification = appStateProvider.notification;
    _checkExistingUnit();
    _checkExistingNotification();
  }

  void _checkExistingUnit() {
    if(_existingUnit == "ImperialUnit") {
      setState(() {
        _isImperialUnit = true;
        _isMetricUnit = false;
      });
    }
    else {
      setState(() {
        _isImperialUnit = false;
        _isMetricUnit = true;
      });
    }
  }

  void _checkExistingNotification() {
    if(_existingNotification == "Yes") {
      setState(() {
        _isNotificationYes = true;
        _isNotificationNo = false;
      });
    }
    else {
      setState(() {
        _isNotificationYes = false;
        _isNotificationNo = true;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    AppStateNotifier appStateProvider = Provider.of<AppStateNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset((appStateProvider.isDarkModeOn) ? ('images/back_button_light.png') : ('images/back_button_dark.png')),
          onPressed: () { Navigator.pop(context); },
        ),
        title: Text("Settings"),
        centerTitle: true,
      ),

      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            //  Unit Container
            Container(
              height: MediaQuery.of(context).size.height/3.3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                  Text(
                    "Calculate Unit",
                    style: Theme.of(context).textTheme.caption,
                  ),

                  SizedBox(height: 20.0,),

                  Text(
                    "Switch betwen Imperial System Unit and Metric SYstem Unit.",
                    style: Theme.of(context).textTheme.body2,
                    textAlign: TextAlign.justify,
                  ),

                  SizedBox(height: 15.0,),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      //  English Unit Column
                      Column(
                        children: <Widget>[
                          InkWell(
                            onTap: () => {
                              _changeUnit(_isImperialUnit),
                              appStateProvider.updateUnit("ImperialUnit"),
                            },
                            child: Card(
                              elevation: 2.0,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 40.0,
                                    width: 70.0,
                                    child: Center(
                                      child: Text(
                                        "lb/ft",
                                        style: Theme.of(context).textTheme.subtitle,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 5.0,
                                    top: 5.0,
                                    child: Visibility(
                                      visible: _isImperialUnit,
                                      child: SizedBox(
                                        child: Image.asset('images/circle_indicator.png'),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),

                          SizedBox(height: 15.0,),

                          Text(
                            "Imperial Unit",
                            style: Theme.of(context).textTheme.subtitle,
                          ),
                        ],
                      ),

                      //  Metric Unit Column
                      Column(
                        children: <Widget>[
                          InkWell(
                            onTap: () => {
                              _changeUnit(_isMetricUnit),
                              appStateProvider.updateUnit("MetricUnit"),
                            },
                            child: Card(
                              elevation: 2.0,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 40.0,
                                    width: 70.0,
                                    child: Center(
                                      child: Text(
                                        "kg/m",
                                        style: Theme.of(context).textTheme.subtitle,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 5.0,
                                    top: 5.0,
                                    child: Visibility(
                                      visible: _isMetricUnit,
                                      child: SizedBox(
                                        child: Image.asset('images/circle_indicator.png'),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),

                          SizedBox(height: 15.0,),

                          Text(
                            "Metric Unit",
                            style: Theme.of(context).textTheme.subtitle,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),

            //  Divider Container
            Container(
              child: Divider(
                height: 1.0,
                color: (appStateProvider.isDarkModeOn) ? (Colors.grey) : (Color(0xFF10093D)),
              ),
            ),

            SizedBox(height: 15.0,),

            //  Notification Container
            Container(
              height: MediaQuery.of(context).size.height/4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Notification",
                    style: Theme.of(context).textTheme.caption,
                  ),

                  SizedBox(height: 20.0,),

                  Text(
                    "Would you like to be getting notifications due to personal upfit and dietories?",
                    style: Theme.of(context).textTheme.body2,
                    textAlign: TextAlign.justify,
                  ),

                  SizedBox(height: 15.0,),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                        onTap: () => {
                          _changeNotification(_isNotificationYes),
                          appStateProvider.updateNotification("Yes"),
                        },
                        child: Card(
                          elevation: 2.0,
                          child: Stack(
                            children: <Widget>[
                              Container(
                                height: 40.0,
                                width: 70.0,
                                child: Center(
                                  child: Text(
                                    "Yes",
                                    style: Theme.of(context).textTheme.subtitle,
                                  ),
                                ),
                              ),
                              Positioned(
                                right: 5.0,
                                top: 5.0,
                                child: Visibility(
                                  visible: _isNotificationYes,
                                  child: SizedBox(
                                    child: Image.asset('images/circle_indicator.png'),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),

                      Column(
                        children: <Widget>[
                          InkWell(
                            onTap: () => {
                              _changeNotification(_isNotificationNo),
                              appStateProvider.updateNotification("No"),
                            },
                            child: Card(
                              elevation: 2.0,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 40.0,
                                    width: 70.0,
                                    child: Center(
                                      child: Text(
                                        "No",
                                        style: Theme.of(context).textTheme.subtitle,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 5.0,
                                    top: 5.0,
                                    child: Visibility(
                                      visible: _isNotificationNo,
                                      child: SizedBox(
                                        child: Image.asset('images/circle_indicator.png'),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
