import 'package:bmi_calculator/app_configs/state_notifier/app_state_notifier.dart';
import 'package:bmi_calculator/data/result_data.dart';
import 'package:bmi_calculator/pages/app_drawer.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../routes/router_names.dart';

class AppHome extends StatefulWidget {
  @override
  _AppHomeState createState() => _AppHomeState();
}

/*
  * https://www.epic4health.com/bmiformula.html
  *
  * IMPERIAL FORMULA
  * BMI = (Weight in Pounds / ((Height in Inches) x (Height in Inches))) * 703
  *
  * METRIC FORMULA
  * BMI = Weight in Kilograms / ((Height in Meters) x (Height in Meters))
  *                     OR
  * BMI = (Weight in Kilograms / ((Height in Centimeters) x (Height in Centimeters))) * 10000
  *
  *
  * MI Categories:
    Underweight = <18.5
    Normal weight = 18.5–24.9
    Overweight = 25–29.9
    Obesity = BMI of 30 or greater
  *
  *
  * */

class _AppHomeState extends State<AppHome> {
  String _selectedGender = 'male';
  bool _setMaleVisible = true;
  bool _setFemaleVisible = false;
  int _age;
  double _height;
  double _weight;
  double _bmiResult = 0.0;
  String _bmiReading = '';
  String _bmiTips = '';
  String _toastMessage = '';
  bool _themeState;
  double _heightSliderValue = 0.0;
  double _minHeightIndicator = 0.0;
  double _maxHeightIndicator = 8.0;

  final TextEditingController _ageController = new TextEditingController();
  final TextEditingController _heightController = new TextEditingController();
  final TextEditingController _weightController = new TextEditingController();

  GlobalKey<ScaffoldState> _scaffoldGlobalKey = new GlobalKey<ScaffoldState>();

  //  Change Weight Function
  void _selectGender(bool setVisibility) {
    if (setVisibility == _setMaleVisible) {
      setState(() {
        _selectedGender = 'male';
        _setMaleVisible = true;
        _setFemaleVisible = false;
      });
    } else {
      setState(() {
        _selectedGender = 'female';
        _setMaleVisible = false;
        _setFemaleVisible = true;
      });
    }
  }

  SnackBar _snackBar(String toastMessage) {
    return new SnackBar(
      content: Text(
        toastMessage,
        style: Theme.of(context).textTheme.body1,
      ),
      backgroundColor: Theme.of(context).colorScheme.background,
      elevation: 15.0,
    );
  }

  //  Decrease Weight Function
  void _decreaseWeight() {
    if (_weight < 2.0) {
      setState(() {
        _toastMessage = 'Weight cannot be less than 1.0';
      });
      _scaffoldGlobalKey.currentState.showSnackBar(_snackBar(_toastMessage));
    } else {
      _weight--;
      _weightController.text = _weight.toString();
    }
  }

  //  Increase Weight Function
  void _increaseWeight() {
    setState(() {
      _weight++;
      _weightController.text = _weight.toString();
    });
  }

  //  Decrease Age Function
  void _decreaseAge() {
    if (_age < 2) {
      setState(() {
        _toastMessage = 'Age cannot be less than 1';
      });
      _scaffoldGlobalKey.currentState.showSnackBar(_snackBar(_toastMessage));
    } else {
      _age--;
      _ageController.text = _age.toString();
    }
  }

  //  Increase Weight Function
  void _increaseAge() {
    setState(() {
      _age++;
      _ageController.text = _age.toString();
    });
  }

  //  Calculate BMI Function
  void _calculateBMI(appStateProvider) {
    setState(() {
      //  Getting All the values of the TextFormFields from their corresponding Controllers
      _age = int.parse(_ageController.text);
      _height = double.parse(_heightController.text);
      _weight = double.parse(_weightController.text);

      double _convertedHeightToInches =
          _height * 12; //  To convert Foot to Inches, multiply by 12.

      //  Validate the TextFields before performing the Calculation
      if ((_ageController.text.isNotEmpty && _age > 0) &&
          (_heightController.text.isNotEmpty && _height > 0.0) &&
          (_weightController.text.isNotEmpty && _weight > 0.0)) {
        if (appStateProvider.selectedUnit == "ImperialUnit") {
          _bmiResult = ((_weight) /
                  (_convertedHeightToInches * _convertedHeightToInches)) *
              703;
        } else {
          _bmiResult = (_weight) / (_height * _height);
        }

        if (_bmiResult < 18.5) {
          _bmiReading = 'Underweight';
          _bmiTips =
              "Eat more frequently: Sometimes, you may feel full faster. Eat smaller meals frequently during the day rather than two or three large meals.";
        } else if (_bmiResult >= 18.5 && _bmiResult < 25.0) {
          _bmiReading = 'Normal';
          _bmiTips =
              "Maintain stady weight: To keep your weight the same, you need to burn the same number of calories as you eat and drink.";
        } else if (_bmiResult >= 25.0 && _bmiResult < 30.0) {
          _bmiReading = 'Overweight';
          _bmiTips =
              "Go Big for Breakfast: People who eat more in the morning and less at night tend to lose more weight.";
        } else {
          _bmiReading = 'Obese';
          _bmiTips =
              "Do Physical Execise: You don't need too much exercise to lose weight on this plan, but it is recommended.";
        }
      }
    });

    var _resultData = new ResultData();
    _resultData.bmiResult = _bmiResult;
    _resultData.bmiTips = _bmiTips;
    _resultData.bmiReading = _bmiReading;

    Navigator.pushNamed(context, bmiResultPage, arguments: _resultData);
  }

  //  Open The Menu Drawer
  void _openDrawer() {
    _scaffoldGlobalKey.currentState.openDrawer();
  }

  //  Height SLider

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    AppStateNotifier appStateProvider =
        Provider.of<AppStateNotifier>(context, listen: false);

    _age = appStateProvider.age;
    _height = appStateProvider.height;
    _weight = appStateProvider.weight;

    _themeState = appStateProvider.isDarkModeOn;
  }

  @override
  Widget build(BuildContext context) {
    AppStateNotifier appStateProvider = Provider.of<AppStateNotifier>(context);
    return Scaffold(
      key: _scaffoldGlobalKey,
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(6.0),
          child: IconButton(
            icon: Image.asset((appStateProvider.isDarkModeOn)
                ? ('images/menu_icon_light.png')
                : ('images/menu_icon_dark.png')),
            onPressed: () {
              _openDrawer();
            },
          ),
        ),
        title: Text("BMI Calculator"),

        /*actions: <Widget>[
          Switch(
            value: _themeState,
              onChanged: (bool boolValue) {
                appStateProvider.updateTheme(boolValue);
              }
          )
        ],*/

        centerTitle: true,
        //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
      ),
      drawer: AppDrawer(),
      body: DoubleBackToCloseApp(
        snackBar: _snackBar("Tap back again to exit app."),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 50.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                //  Male & Female
                Container(
                  height: MediaQuery.of(context).size.height / 4,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(
                      left: 0.0, top: 30.0, right: 0.0, bottom: 0.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      InkWell(
                        onTap: () => {_selectGender(_setMaleVisible)},
                        child: Card(
                          elevation: 1.0,
                          margin: EdgeInsets.all(0.0),
                          child: Stack(
                            children: <Widget>[
                              Positioned(
                                right: 7.0,
                                top: 7.0,
                                child: Visibility(
                                  maintainSize: true,
                                  maintainAnimation: true,
                                  maintainState: true,
                                  visible: _setMaleVisible,
                                  child: SizedBox(
                                    child: Image.asset(
                                        'images/circle_indicator.png'),
                                  ),
                                ),
                              ),
                              Container(
                                height: 77.0,
                                width: 65.0,
                                alignment: Alignment.center,
                                margin: EdgeInsetsDirectional.only(
                                    start: 23.0,
                                    top: 19.0,
                                    end: 23.0,
                                    bottom: 10.0),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 36.15,
                                      height: 34.56,
                                      child: Image.asset(
                                          (appStateProvider.isDarkModeOn)
                                              ? ('images/male_light_icon.png')
                                              : ('images/male_dark_icon.png')),
                                    ),
                                    Padding(padding: EdgeInsets.all(7.0)),
                                    Text(
                                      'Male',
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () => {_selectGender(_setFemaleVisible)},
                        child: Card(
                          elevation: 1.0,
                          margin: EdgeInsets.all(0.0),
                          child: Stack(
                            children: <Widget>[
                              Positioned(
                                right: 7.0,
                                top: 7.0,
                                child: Visibility(
                                  maintainSize: true,
                                  maintainAnimation: true,
                                  maintainState: true,
                                  visible: _setFemaleVisible,
                                  child: SizedBox(
                                    width: 10.0,
                                    height: 10.0,
                                    child: Image.asset(
                                        'images/circle_indicator.png'),
                                  ),
                                ),
                              ),
                              Container(
                                height: 77.0,
                                width: 65.0,
                                alignment: Alignment.center,
                                margin: EdgeInsetsDirectional.only(
                                    start: 23.0,
                                    top: 19.0,
                                    end: 23.0,
                                    bottom: 10.0),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 36.15,
                                      height: 34.56,
                                      child: Image.asset((appStateProvider
                                              .isDarkModeOn)
                                          ? ('images/female_light_icon.png')
                                          : ('images/female_dark_icon.png')),
                                    ),
                                    Padding(padding: EdgeInsets.all(7.0)),
                                    Text('Female',
                                        style:
                                            Theme.of(context).textTheme.caption)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                //  Height Container
                Container(
                  height: MediaQuery.of(context).size.height / 4,
                  width: MediaQuery.of(context).size.width,
                  padding:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "HEIGHT",
                        style: Theme.of(context).textTheme.subtitle,
                      ),
                      Container(
                        height: 70.0,
                        width: 90.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              height: 70.0,
                              width: 75.0,
                              child: TextField(
                                controller: _heightController,
                                keyboardType: TextInputType.number,
                                style: Theme.of(context).textTheme.display3,
                                textAlign: TextAlign.center,
                                cursorColor:
                                    Theme.of(context).colorScheme.background,
                                decoration: InputDecoration(
                                  hintText: (appStateProvider.selectedUnit ==
                                          "ImperialUnit")
                                      ? ("5.6")
                                      : ("1.7"),
                                  hintStyle:
                                      Theme.of(context).textTheme.display2,
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            Text(
                              (appStateProvider.selectedUnit == "ImperialUnit")
                                  ? ("ft")
                                  : ("m"),
                              style: Theme.of(context).textTheme.subtitle,
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 20.0,
                        child: Slider(
                          value: appStateProvider.height,
                          min: _minHeightIndicator,
                          max: _maxHeightIndicator,
                          activeColor: Colors.pinkAccent,
                          inactiveColor: Colors.grey,
                          onChanged: (newHeightValue) {
                            appStateProvider.updateHeightValue(newHeightValue);
                          },
                        ),
                      ),
                    ],
                  ),
                ),

                //  Weight & Age Row
                Container(
                  height: MediaQuery.of(context).size.height / 4,
                  width: MediaQuery.of(context).size.width,
                  padding:
                      EdgeInsets.symmetric(vertical: 15.0, horizontal: 0.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      //  Weight Container
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "WEIGHT",
                              style: Theme.of(context).textTheme.subtitle,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 95.0,
                                  child: TextField(
                                    controller: _weightController,
                                    keyboardType: TextInputType.number,
                                    style: Theme.of(context).textTheme.display3,
                                    textAlign: TextAlign.center,
                                    cursorColor: Theme.of(context)
                                        .colorScheme
                                        .background,
                                    decoration: InputDecoration(
                                      hintText:
                                          (appStateProvider.selectedUnit ==
                                                  "ImperialUnit")
                                              ? ("143.3")
                                              : ("65.0"),
                                      hintStyle:
                                          Theme.of(context).textTheme.display2,
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                                Text(
                                  (appStateProvider.selectedUnit ==
                                          "ImperialUnit")
                                      ? ("lb")
                                      : ("kg"),
                                  style: Theme.of(context).textTheme.subtitle,
                                )
                              ],
                            ),
                            Container(
                              width: 100.0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  InkWell(
                                    onTap: () => {_decreaseWeight()},
                                    child: Container(
                                      height: 40.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .background,
                                      ),
                                      child: Image.asset(
                                          (appStateProvider.isDarkModeOn)
                                              ? ("images/subtract_light.png")
                                              : ("images/subtract_dark.png")),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () => {_increaseWeight()},
                                    child: Container(
                                      height: 40.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .background,
                                      ),
                                      child: Image.asset(
                                          (appStateProvider.isDarkModeOn)
                                              ? ("images/add_light.png")
                                              : ("images/add_dark.png")),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      //  Age Container
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "AGE",
                              style: Theme.of(context).textTheme.subtitle,
                            ),
                            Container(
                              width: 115.0,
                              child: TextField(
                                controller: _ageController,
                                keyboardType: TextInputType.number,
                                style: Theme.of(context).textTheme.display3,
                                textAlign: TextAlign.center,
                                cursorColor:
                                    Theme.of(context).colorScheme.background,
                                decoration: InputDecoration(
                                  hintText: "32",
                                  hintStyle:
                                      Theme.of(context).textTheme.display2,
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            Container(
                              width: 100.0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  InkWell(
                                    onTap: () => {_decreaseAge()},
                                    child: Container(
                                      height: 40.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .background,
                                      ),
                                      child: Image.asset(
                                          (appStateProvider.isDarkModeOn)
                                              ? ("images/subtract_light.png")
                                              : ("images/subtract_dark.png")),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () => {_increaseAge()},
                                    child: Container(
                                      height: 40.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .background,
                                      ),
                                      child: Image.asset(
                                          (appStateProvider.isDarkModeOn)
                                              ? ("images/add_light.png")
                                              : ("images/add_dark.png")),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                //  Calculate Button
                Container(
                  height: MediaQuery.of(context).size.height / 7,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 260,
                    height: 52,
                    child: RaisedButton(
                      elevation: 5.0,
                      onPressed: () => {_calculateBMI(appStateProvider)},
                      child: Text(
                        'Calculate BMI'.toUpperCase(),
                        style: Theme.of(context).textTheme.button,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
