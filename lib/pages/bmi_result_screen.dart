import 'package:flutter/material.dart';
import 'package:bmi_calculator/app_configs/state_notifier/app_state_notifier.dart';

import 'package:bmi_calculator/data/result_data.dart';
import 'package:bmi_calculator/routes/router_names.dart';
import 'package:provider/provider.dart';


class BMIResult extends StatelessWidget {

  final ResultData routerArguments;

  BMIResult({ Key key, this.routerArguments }) : super(key: key);


  void _showHintPage(context) {
    var _resultData = new ResultData();
    _resultData.bmiReading = routerArguments.bmiReading;

    Navigator.pushNamed(context, bmiHintPage, arguments: _resultData);
  }

  @override
  Widget build(BuildContext context) {
    AppStateNotifier appStateProvider = Provider.of<AppStateNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset((appStateProvider.isDarkModeOn) ? ('images/back_button_light.png') : ('images/back_button_dark.png')),
          onPressed: () { Navigator.pop(context); },
        ),
        title: Text("BMI Result"),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 25.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              //  Your Result Title
              Container(
                height: MediaQuery.of(context).size.height/8,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.centerLeft,
                child: Text(
                    'Your Result',
                    style: Theme.of(context).textTheme.display4
                ),
              ),

              //  The Transparent Card
              Flexible(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 20.0),
                  alignment: Alignment.center,
                  child: Card(
                    elevation: 1.0,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 35.0, horizontal: 10.0),
                      child: Column(
                        children: <Widget>[

                          //  BMI Reading
                          Text(
                            routerArguments.bmiReading.toUpperCase(),
                            style: TextStyle(
                              color: (routerArguments.bmiReading == "Underweight") ? Color(0xFF1565C0) : (routerArguments.bmiReading == "Normal") ? Color(0xFF06FF1F) : (routerArguments.bmiReading == "Overweight") ? Color(0xFFF57C00) : Color(0xFFF44336),
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Raleway',
                            ),
                          ),
                          Padding(padding: EdgeInsets.all(10.0)),

                          //  BMI Result
                          Text(
                            ((routerArguments.bmiResult * 10).roundToDouble()/10).toString(),
                            style: Theme.of(context).textTheme.display1,
                          ),
                          Padding(padding: EdgeInsets.all(11.0)),

                          //  Range:
                          Text(
                            (routerArguments.bmiReading == "Underweight") ? 'Range:\n0 - 18.5 kg/m2' : (routerArguments.bmiReading == "Normal") ? 'Range:\n18.5 - 25 kg/m2' : (routerArguments.bmiReading == "Overweight") ? 'Range:\n25 - 29.9 kg/m2' : 'Range:\n> 30 kg/m2',
                            style: Theme.of(context).textTheme.subtitle,
                            textAlign: TextAlign.center,
                          ),
                          Padding(padding: EdgeInsets.all(11.0)),

                          //  BMI Hint
                          Text(
                            routerArguments.bmiTips,
                            style: Theme.of(context).textTheme.body2,
                            textAlign: TextAlign.justify,
                          ),
                          Padding(padding: EdgeInsets.all(5.0)),

                          //  More Hints
                          Align(
                            alignment: Alignment.bottomRight,
                            child: InkWell(
                              // onTap: () {Navigator.push(context, MaterialPageRoute(builder: (context) => BMIHint(bmiReader: routerArguments.bmiReading)));},
                              onTap: () { _showHintPage(context); },
                              child: Text(
                                'more hints',
                                style: Theme.of(context).textTheme.body1,
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.all(8.0)),

                          //  Save Result
                         /* Text(
                            'Save Result',
                            style: Theme.of(context).textTheme.caption,
                          ),*/
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              //  ReCalculate Button
              Container(
                height: MediaQuery.of(context).size.height/6,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                child: SizedBox(
                  width: 260,
                  height: 52,
                  child: RaisedButton(
                    elevation: 5.0,
                    onPressed: () => { Navigator.pushNamed(context, appHomePage) },
                    child: Text(
                      'Home'.toUpperCase(),
                      style: Theme.of(context).textTheme.button,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

