import 'package:flutter/material.dart';
import 'package:bmi_calculator/app_configs/state_notifier/app_state_notifier.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:bmi_calculator/routes/router_names.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {

  bool _themeState;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    AppStateNotifier appStateProvider = Provider.of<AppStateNotifier>(context);
    _themeState = appStateProvider.isDarkModeOn;
  }

  @override
  Widget build(BuildContext context) {
    AppStateNotifier appStateProvider = Provider.of<AppStateNotifier>(context);
    return Drawer(
      elevation: 16.0,
      child: Container(
        color: Theme.of(context).colorScheme.primaryVariant,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  DrawerHeader(
                    child: Center(
                      child: CircleAvatar(
                        backgroundColor: Theme.of(context).colorScheme.background,
                        radius: 40.0,
                        child: Text("Logo", style: Theme.of(context).textTheme.caption,),
                      ),
                    ),
                  ),
                  InkWell(
                    child: ListTile(
                      dense: true,
                      leading: Image.asset("images/home_icon.png"),
                      title: Text(
                        "Home",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                    onTap: () { Navigator.of(context).pop(); },
                  ),
                  InkWell(
                    child: ListTile(
                      dense: true,
                      leading: Image.asset("images/save_icon.png"),
                      title: Text(
                        "Save Result",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                    onTap: () { Navigator.of(context).pop(); },
                  ),
                  InkWell(
                    child: ListTile(
                      dense: true,
                      leading: Image.asset("images/theme_icon.png"),
                      title: Text(
                        "Dark Theme",
                        style: Theme.of(context).textTheme.caption,
                      ),
                      trailing: Switch(
                          value: _themeState,
                          onChanged: (bool boolValue) {
                            appStateProvider.updateTheme(boolValue);
                          }),
                    ),
                    //onTap: () {Navigator.of(context).pop();},
                  ),
                  InkWell(
                    child: ListTile(
                      dense: true,
                      leading: Image.asset("images/settings_icon.png"),
                      title: Text(
                        "Setting",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, appSettings);
                      },
                  ),
                  InkWell(
                    child: ListTile(
                      dense: true,
                      leading: Image.asset("images/information_icon.png"),
                      title: Text(
                        "Notification",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ),
                    onTap: () {Navigator.of(context).pop();},
                  ),
                ],
              ),
            ),
            InkWell(
              child: ListTile(
                leading: Image.asset("images/exit_icon.png"),
                title: Text(
                  "Exit",
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
              onTap: () {
                //Navigator.of(context).pop();
                AlertDialog exitAlertDialog = AlertDialog(
                  title: Text(
                    "Exit Application?",
                    style: Theme.of(context).textTheme.caption,
                  ),
                  content: Text(
                    "Sure you want to exit this application?",
                    style: Theme.of(context).textTheme.body1,
                  ),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () => { Navigator.of(context, rootNavigator: true).pop() },
                      child: Text("Cancel",
                        style: Theme.of(context).textTheme.body1,
                      ),
                    ),
                    FlatButton(
                      onPressed: () => { SystemChannels.platform.invokeMethod('SystemNavigator.pop') },
                      child: Text("Accept",
                        style: Theme.of(context).textTheme.body1,
                      ),
                    ),
                  ],
                  elevation: Theme.of(context).cardTheme.elevation,
                  backgroundColor: Colors.grey,
                );
                showDialog(
                  context: context,
                  builder: (context) => exitAlertDialog,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}


