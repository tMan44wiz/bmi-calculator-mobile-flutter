import 'package:bmi_calculator/app_configs/state_notifier/app_state_notifier.dart';
import 'package:bmi_calculator/data/result_data.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BMIHint extends StatefulWidget {
  final ResultData routerArgument;

  BMIHint({Key key, this.routerArgument}) : super(key: key);

  @override
  _BMIHintState createState() => _BMIHintState();
}

class _BMIHintState extends State<BMIHint> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _checkHint();
  }

  List<String> detailedHints = [];

  _checkHint() {
    String _bmiReading = "${widget.routerArgument.bmiReading}";
    if (_bmiReading == "Underweight") {
      setState(() {
        detailedHints = [
          "Eat more frequently: When you're underweight, you may feel full faster. Eat five to six smaller meals during the day rather than two or three large meals.",
          "Choose nutrient-rich foods: As part of an overall healthy diet, choose whole-grain breads, pastas and cereals; fruits and vegetables; dairy products; lean protein sources; and nuts and seeds.",
          "Top it off: Add extras to your dishes for more calories such as cheese in casseroles and scrambled eggs, and fat-free dried milk in soups and stews",
          "Try smoothies and shakes: Don't fill up on diet soda, coffee and other drinks with few calories and little nutritional value. Instead, drink smoothies or healthy shakes made with milk and fresh or frozen fruit, and sprinkle in some ground flaxseed.",
        ];
      });
    } else if (_bmiReading == "Normal") {
      setState(() {
        detailedHints = [
          "Maintain steady weight: To keep your weight the same, you need to burn the same number of calories as you eat and drink.",
          "Go Big for Breakfast: People who eat more in the morning and less at night tend to lose more weight. Some studies suggest that starting your day with a high-protein meal especially warm, solid food helps you feel fuller and less hungry later.",
          "Maintain physical Exercise: Be as physically active as you can be. Do not starve yourself in the name that you are watching your weight.",
          "Contact Medical Specialist: Talk to your doctor about your weight if you think that you weigh too much or too little.",
        ];
      });
    } else if (_bmiReading == "Overweight") {
      setState(() {
        detailedHints = [
          "Go Big for Breakfast: People who eat more in the morning and less at night tend to lose more weight. Some studies suggest that starting your day with a high-protein meal especially warm, solid food helps you feel fuller and less hungry later.",
          "Get in the Pool (If you Can Swim): Swimming is a whole-body, non-impact workout with a fantastic calorie burn. The water helps hold you up, so there's no pressure on your joints. Plus, it saves time by combining cardio and muscle-building in a single activity.",
          "Exercise daily: A minimum of 30-45 minutes daily @ 5-6km/hr for people below 40 years and free of any disease. Children need to be exposed to a lot of physical activity.",
          "Drink Water Frequently: It is often claimed that drinking water can help with weight loss and that’s true. Drinking water can boost metabolism by 24–30% over a period of 1–1.5 hours, helping you burn off a few more calories.",
          "Eat Less Refined Carbohydrates: Refined carbohydrates include sugar and grains that have been stripped of their fibrous, nutritious parts. These include white bread and pasta. If you're going to eat carbs, make sure to eat them with their natural fiber.",
        ];
      });
    } else {
      setState(() {
        detailedHints = [
          "Do Physical Exercise: You don't need too much exercise to lose weight on this plan, but it is recommended. The best option is to go to the gym 3–4 times a week. Do a warm-up and lift some weights. If you're new to the gym, ask a trainer for some advice.",
          "Drink Water Frequently: It is often claimed that drinking water can help with weight loss and that’s true. Drinking water can boost metabolism by 24–30% over a period of 1–1.5 hours, helping you burn off a few more calories.",
          "Eat Less Refined Carbs: Refined carbohydrates include sugar and grains that have been stripped of their fibrous, nutritious parts. These include white bread and pasta. If you're going to eat carbs, make sure to eat them with their natural fiber.",
          "Eat Only when Needed: Parents, please encourage your children to eat only when hungry, and to eat slowly. Avoid forcing food on your children, using food as a reward or withholding food as a punishment."
        ];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    AppStateNotifier appStateProvider = Provider.of<AppStateNotifier>(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset((appStateProvider.isDarkModeOn)
              ? ('images/back_button_light.png')
              : ('images/back_button_dark.png')),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("BMI Calculator"),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.all(15.0)),

            //  Your Result
            Text('Hints', style: Theme.of(context).textTheme.display4),

            Padding(padding: EdgeInsets.all(10.0)),

            //  Hints
            Column(
              children: detailedHints.map((eachHint) {
                return Container(
                  padding: EdgeInsets.only(
                      left: 0.0, top: 0.0, right: 0.0, bottom: 10.0),
                  child: Card(
                    elevation: 2.0,
                    color: Theme.of(context).colorScheme.primaryVariant,
                    borderOnForeground: true,
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      child: Text(
                        eachHint,
                        style: Theme.of(context).textTheme.body2,
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ),
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }
}
