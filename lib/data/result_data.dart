class ResultData {
  String bmiReading;
  String bmiTips;
  double bmiResult;

  ResultData({ this.bmiReading, this.bmiTips, this.bmiResult });
}